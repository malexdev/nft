/* global jot: true, _: true, util: true, settings: true, fs: true, sessionStore: true, log: true, async: true, path: true */ /* jshint strict: false */
console.log('Starting server');
jot = require('json-over-tcp');
_ = require('underscore');
util = require('util');
async = require('async');
path = require('path');
fs = require('fs-extra');
settings = require('./settings.json');
sessionStore = require('./controllers/session');
log = require('./log');
var route = require('./controllers/router');
var subscriptions = require('./controllers/subscriptions');

// start the server
var server = jot.createServer(settings.server.port);

// fired when a new client connects
server.on('connection', function(socket) {
    log.write('New client connected from address ' + socket.remoteAddress);
    var thisSession = new sessionStore.Session();
    
    // create a new session for this client
    sessionStore.createSession(socket.remoteAddress, function(session) {
        log.write('Created new session for client: ' + JSON.stringify(session), socket);
        thisSession = session;
    });
    
    // fired when the socket sends data to the server 
    socket.on('data', function(message) {
        log.write('Received message from client: ' + JSON.stringify(message), socket);
        // get the session for this client
        
        route(message, thisSession, function(response) {
            socket.write(response);
            if (message.dl) {
                log.write('Sent file to client: ' + message.dl, socket);
            } else {
                log.write('Sent response to client: ' + JSON.stringify(response), socket);   
            }
        });
    });
    
    // fired when the socket sends a 'fin' packet
    socket.on('end', function() {
        log.write('Client disconnected');
        sessionStore.deleteSession(thisSession, function(success) {
            log.write(success ? 'Deleted session' : 'Unable to delete session');
        });
    });
    
    // fired when the socket times out
    socket.on('timeout', function() {
        log.write('Client timed out', socket);
    });
    
    // fired when the socket has an error
    socket.on('error', function(err) {
        log.write('Socket error: ' + err.toString());
    });
    
    // fired when the remote socket is closed
    socket.on('close', function(hadError) {
        log.write('Socket closed' + (hadError ? ' (with error)':''));
    });
});

// fired when the server begins listening
server.on('listening', function() {
    log.write('Server running on port ' + settings.server.port);
});

// fired when the server stops listening
server.on('close', function() {
    log.write('Server shut down');
});

// fired when the server has an error binding listen port
server.on('error', function(err) {
    log.write('Server error: ' + err.toString());
});

// start the server
server.listen(settings.server.port);