var util = require('util');

var log = {
    write: function(text, socket) {
        if (socket) { text = util.format('[%s] %s', socket.remoteAddress, text); }
        console.log(text);
    }
};

module.exports = log;