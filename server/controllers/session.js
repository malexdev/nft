/* global log, sessionStore, fs, _, util, async, path, settings */ 'use strict';
var tilde = require('tilde-expansion');

// set up variables for this controller
var sessions = [];

// set up regex we'll be using
var rxIpAddress = /\b((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.|$)){4}\b/;
var rxTildePath = /~/g;

// define what a session object is
var Session = function(address) {
    this.address = address || '127.0.0.1';
    this.authorized = false;
};

// validate and set the IP address of the session
Session.prototype.setAddress = function(newAddress) {
    // ensure that the address matches an IP address
    if (newAddress.match(rxIpAddress)) {
        this.address = newAddress;
    } else {
        log.write(util.format('IP Address "%s" is not a valid IP', newAddress));
        return false;
    }
};

// get the session for a given client
var getSession = function(address, callback) {
    var foundSession = false;
    sessions.forEach(function(session) { 
        if (session.address === address) { 
            foundSession = true;
            return callback(session); 
        } 
    });
    
    // if we couldn't find one that matches, create a new session and return it
    if (!foundSession) {
        createSession(address, function(session) { callback(session); });
    }
};

// create the session, push it to the store, and return it
var createSession = function(address, callback) {
    var session = new Session(address);
    
    // first, delete any old sessions with the same address laying around
    deleteSession(session, function(finished) {
        // now add the new session
        sessions.push(session);
        return callback(session);  
    });
};

// delete the session
var deleteSession = function(session, callback) {
    if (!session) { return callback(true); }
    log.write('Deleting session: ' + JSON.stringify(session));
    sessions = _.reject(sessions, function(iterator) { return iterator.address === session.address; });
    return callback(true);
};

// authorize a specific session
var authorizeSession = function(session, password, callback) {
    // verify that the password is correct
    if (password === settings.server.password) {
        // find the session in question, update its authorized flag, and return it
        var target = _.find(sessions, function(iterator) { return iterator.address === session.address; });
        var index = sessions.indexOf(target);
        // set the session to true
        target.authorized = true;
        sessions[index] = target;
        // return the session
        return callback(target);
    } else {
        // tell the caller that the session wasn't authorized
        return callback(false);
    }
};

// export things
module.exports.Session = Session;
module.exports.getSession = getSession;
module.exports.createSession = createSession;
module.exports.deleteSession = deleteSession;
module.exports.authorizeSession = authorizeSession;