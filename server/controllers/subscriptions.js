/* global log, sessionStore, fs, _, util, async, path, settings */
'use strict';

// includes
var fsmon = require('fsmonitor');

// set up scoped subscription array
var allSubscriptions = [];

// set up the debug subsription
var debugSub = new Subscription();
debugSub.path = '/Users/alexmartin/Desktop';
debugSub.subscribers = ['127.0.0.1'];
allSubscriptions.push(debugSub);

// begin watching the subscriptions
beginWatchingSubscriptions();

function beginWatchingSubscriptions() {
    log.write('Beginning to watch subscriptions');
    
    allSubscriptions.forEach(function(subscription) {
        fs.exists(subscription.path, function(exists) {
            if (!exists) {
                log.write('Could not begin watching ' + subscription.path + ' as it does not exist');
            } else {
                // now start watching the file
                log.write('Adding watch for path: ' + subscription.path);
                fsmon.watch(subscription.path, null, subscriptionChanged);
            }
        });
    });
}

function subscriptionChanged(change) {
    log.write('Subscription change event: ' + change);
}

function addSubscription(session, path, callback) {
    log.write('Adding subscription to ' + path, session);

    // check if we already have a subscription for the specified path
    var subIndex = findIndexOfSubscription(path);

    // if we already have a subscription to this path, add the new session to it
    if (subIndex !== -1) {

        // add the new subscribers address if it isn't already there
        if (!_.contains(allSubscriptions[subIndex].subscribers, session.address)) {
            
            // push the new subscriber onto the subscriber list
            allSubscriptions[subIndex].subscribers.push(session.address);    
        }
        
        // run the callback
        return callback();
        
    } else { // we don't have a subscription for this path yet, so create a new one
        var newSub = new Subscription();
        newSub.path = path;
        newSub.subscribers = [session.address];
        
        // push it onto the subscription list
        allSubscriptions.push(newSub);
        
        // run the callback
        return callback();
    }
}

function removeSubscription(session, path, callback) {
    log.write('Removing subscription for ' + path, session);
    
    // check if there is a subscription for that path
    var subIndex = findIndexOfSubscription(path);
    
    if (subIndex !== -1) {
        
        // get the index of this session in the subscription list for this path
        var sessionIndex = _.indexOf(allSubscriptions[subIndex].subscribers, session.address);
        
        // splice the array
        if (sessionIndex !== -1) { allSubscriptions[subIndex].subscribers.splice(sessionIndex, 1); }
        return callback();
    } else {
        return callback();
    }
}

function removeAllSubscriptionsForSession(session, callback) {
    log.write('Removing all subscriptions', session);
    
    // go through each subscription
    for (var i = 0; i < allSubscriptions.length; i++) {
        
        // check if the subscription contains this session
        var targetIndex = _.indexOf(allSubscriptions[i].subscribers, session.address);
        
        // if it's there, splice it out
        if (targetIndex !== -1) {
            allSubscriptions[i].subscribers.splice(targetIndex, 1);
        }
    }
    
    // once we're done, return the callback
    return callback();
}

function findIndexOfSubscription(path) {
    for (var i = 0; i < allSubscriptions.length; i++) {
        var sub = allSubscriptions[i];
        if (sub.path === path) { return i; }
    }
    return -1;
}

function Subscription() {

    // the path for the file/folder that is to be watched
    this.path = '';
    
    // the checksum of the path the last time it was checked
    this.checksum = '';
    
    // the time the file was last checked
    this.checkedTime = null;

    // the session addresses subscribed to this file
    this.subscribers = [];

    return this;
}

module.exports.addSubscription = addSubscription;
module.exports.removeSubscription = removeSubscription;
module.exports.removeAllSubscriptionsForSession = removeAllSubscriptionsForSession;