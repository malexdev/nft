/* global log, sessionStore, fs, _, util, async, path, settings */ 'use strict';
var targz = require('tar.gz');

// set up standard messages
var standardMessages = {
    notAuthorzed: { error: 'You are not authorized to connect to this server' },
    invalidInput: { error: 'The input provided was invalid' },
    fileNotExist: { error: 'The file requested does not exist' }
};

var rx = {
    isNftDirectory: /^\/tmp\/nft-directories.+\.tar\.gz/
};

// route a message
var route = function(message, session, callback) {
    log.write(util.format('Routing message "%s" on session "%s"', JSON.stringify(message), JSON.stringify(session)));
    
    // if the client wants to authorize
    if (message.authorize) { handleAuthRequest(session, message, callback); }
    
    // get the contents of the directory
    if (message.ls) { handleLS(session, message, callback); }
    
    // serve a file to the client
    if (message.dl) { handleDL(session, message, callback); }
    
    // save a file sent from the client
    if (message.ul) { handleUL(session, message, callback); }
    
    if (message.stat) { handleInfoRequest(session, message, callback); }
};

function handleAuthRequest(session, message, callback) {
    log.write(util.format('Attempting to authorize session'));
    // ask the session store to authorize the session
    sessionStore.authorizeSession(session, message.authorize, function(authorized) {
        log.write('Authorization response: ' + (authorized ? 'true' : 'false'));
        // report the session store's result
        return callback({authorized: (authorized ? true : false)});
    });
}

function handleInfoRequest(session, message, callback) {
    log.write(util.format('Getting info for file: %s', message.stat));
    if (!session.authorized) {
        log.write('The session is not authorized');
        return callback(standardMessages.notAuthorzed);
    }
    
    fs.exists(message.stat, function(exists) {
        if (!exists) { return callback(standardMessages.fileNotExist); }
        fs.stat(message.stat, function(err, stats) {
            if (err) { return callback({error: err.toString()}); }
            
            // add extra things to the object
            var statClone = {
                size: stats.size,
                last_access: stats.atime,
                created: stats.ctime,
                last_modified: stats.mtime,
                is_file: stats.isFile(),
                is_directory: stats.isDirectory()
            };
            
            callback(statClone);
        });
    });
}

function handleUL(session, message, callback) {
    log.write('Uploading file to ' + message.ul);
    if (!session.authorized) {
        log.write('The session is not authorized');
        return callback(standardMessages.notAuthorzed);
    }

    // ensure that we have all the data we need
    if (!message.data) {
        log.write('Not passed any data to upload');
        return callback(standardMessages.invalidInput);
    }

    // set defaults
    var encoding = message.encoding || 'utf8';

    if (message.directory) {
        // we need to untar the file to the path specified. First, save to a temp directory
        getTmpStr(function(savePath) {
            route({ul: savePath, data: message.data, encoding: encoding, directory: false}, session, function(result) {
                if (result.error) {
                    log.write('Unable to save file to temp directory: ' + result.error);
                    return callback({error: result.error});
                }

                // extract the saved file
                var compress = targz().extract(savePath, message.ul, function(err) {
                    if (err) {
                        log.write('Unable to extract compressed archive: ' + err.toString());
                        return callback({error: err.toString()});
                    }

                    log.write('Extracted compressed archive');
                    callback({uploaded: true});
                });
            });
        });

    } else {
        // read the data passed and write it to disk. Back up the file if it already exists.
        fs.exists(message.ul, function(exists) {
            // back up the original
            var chunks = chunkFilePath(message.ul);
            var filePath = util.format('%s/.backup-%s', chunks.path, chunks.name);
            log.write('Backing up ' + message.ul + ' to ' + filePath);
            fs.rename(message.ul, filePath, function(err) {
                if (err) {
                    log.write('Unable to rename the file being uploaded to: ' + err.toString());
                    return callback({error: err.toString()});
                }

                // write the file
                fs.writeFile(message.ul, message.data, {encoding: encoding}, function(err) {
                    if (err) {
                        log.write('Unable to write file: ' + err.toString());
                        return callback({error: err.toString()});
                    }

                    // notify the user that we've saved the file
                    log.write('Saved uploaded file successfully');
                    callback({uploaded: true});
                });
            });
        });
    }
}

function handleDL(session, message, callback) {
    if (!session.authorized) {
        log.write('The session is not authorized');
        return callback(standardMessages.notAuthorzed);
    }

    log.write('Trying to send file to client: ' + message.dl);
    // check if the file exists
    fs.exists(message.dl, function(exists) {
        if (exists) {
            // check if the file is a folder
            fs.lstat(message.dl, function(err, stats) {
                if (err) {
                    log.write('Unable to check if path is a folder: ' + err.toString());
                    callback({error: err.toString()});
                } else {
                    if (stats.isDirectory()) {
                        log.write('Path is a directory, compressing');
                        // zip the folder into the /tmp directory
                        getTmpStr(function(savePath) {
                            var compress = new targz().compress(message.dl, savePath, function(err) {
                                if (err) {
                                    log.write('Unable to compress folder: ' + err.toString());
                                    callback({error: err.toString()});
                                } else {
                                    // run self again with the new path, preserving the session and callback. Nifty!
                                    route({dl: savePath}, session, callback);
                                }
                            });
                        });
                    } else {
                        // read the file
                        fs.readFile(message.dl, {encoding: 'base64'}, function(err, data) {
                            if (err) {
                                log.write('Unable to read file: ' + err.toString());
                                callback({error: err.toString()});
                            } else {
                                log.write('Read file, sending to client');
                                callback({file: data, path: message.dl, directory: (message.dl.match(rx.isNftDirectory) ? true : false)});

                                // if the file is in /tmp, delete it
                                if (message.dl.match(rx.isNftDirectory)) { fs.unlink(message.dl); }
                            }
                        });
                    }
                }
            });
        } else {
            log.write('The file requested does not exist');
            callback(standardMessages.fileNotExist);
        }
    });
}

function handleLS(session, message, callback) {
    if (!session.authorized) {
        log.write('The session is not authorized');
        return callback(standardMessages.notAuthorzed);
    }

    log.write('Getting contents for directory ' + message.ls);
    // get the contents of the folder at the specified path
    fs.readdir(message.ls, function(err, files) {
        // check for errors
        if (err) { return callback(standardMessages.invalidInput); }
        // if the user doesn't want to show hidden, filter things out that are hidden
        if (!message.showHidden) { files = _.reject(files, function(iterator) { return iterator.match(/^\./); }); }
        // return the list of files
        return callback(files);
    });
}

function chunkFilePath(chunkPath) {
    return {name: path.basename(chunkPath), path: path.dirname(chunkPath)};
}

function getTmpStr(callback) {
    var fileName = '/tmp/nft-directories/' + Math.random().toString(36).slice(2) + '.tar.gz';
    fs.exists(fileName, function(exists) {
        if (exists) {
            getTmpStr(callback);
        } else {
            callback(fileName);
        }
    });
}

// export things
module.exports = route;