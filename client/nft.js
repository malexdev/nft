console.log('Starting client');

// load things we require
log = require('./log.js');
jot = require('json-over-tcp');
ping = require('tcp-ping');
_ = require('underscore');
settings = require('./settings.json');

// set up starting variables
socket = null;
reconnect = settings.reconnect.starting_ms;

// send the given object to the server
send = function(obj) {
    var result = 'Sent message to server';
    if (socket) {
        socket.write(obj);
    } else {
        result = 'Unable to send object; server not connected';
    }
    return result;
}

// start the repl after 1 second (if enabled)
if (settings.enable_repl) {
    setTimeout(function() {
        var repl = require('repl');

        // start the repl
        var replHost = repl.start({
            prompt: "nft-client > ",
            input: process.stdin,
            output: process.stdout
        });

        // close the client if the user kills the repl
        replHost.on('exit', function() {
            log.write('User killed REPL, closing client');
            process.exit();
        });

    }, 1000);
}

// ping the server every 10 seconds to get our current latency
if (settings.check_latency) {
    setInterval(function() {
        ping.ping({address: settings.server.address, port: settings.server.port}, function(err, data) {
            if (!err) { log.write('Server latency: ' + Math.round(data.avg) + 'ms'); }
        });
    }, 10000);
}

// connect to the server
connect = function() {
    socket = jot.connect({port: settings.server.port, host: settings.server.address}, function() {
        log.write('Connected to server', socket);
        
        // reset our reconnect timer
        reconnect = settings.reconnect.starting_ms;
        
        // authorize to the server
        socket.write({authorize: settings.server.password});
        log.write('Authorized to server');

        // when the server sends us data
        socket.on('data', function(data) {
            log.write('Received data from server: ' + JSON.stringify(data));
        });

        // when the server ends the connection
        socket.on('end', function() {
            log.write('Server closed the connection');
        });

        // when the socket times out due to lack of communication
        socket.on('timeout', function() {
            log.write('Server connection timed out');
        });

    });

    socket.on('error', function(err) {
        // only show the error if we're not repeatedly trying to reconnect after dropping a connection
        if (reconnect === settings.reconnect.starting_ms) { log.write('Unable to connect to server. ' + err.toString()); }
    });

    socket.on('close', function(hadError) {
        // only show the error if we're not repeatedly trying to reconnect after dropping a connection
        if (reconnect === settings.reconnect.starting_ms) { log.write('Connection to server closed' + (hadError ? ' (with error)':'')); }
        
        // try to reconnect to the server
        reconnect = Math.round(reconnect * (settings.reconnect.multiplier_percent / 100));
        if (reconnect > settings.reconnect.maximum_ms) { reconnect = settings.reconnect.maximum_ms; }
        setTimeout(connect, reconnect);
        log.write('Trying to reconnect in ' + reconnect + 'ms');
    });
};

// start the client
connect();