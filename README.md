I’ve been working on this off and on for a little while, but I figured
it was about time to put it on source control. It’s still nowhere near
finished.

## What is it

Node File Transfer: FTP, Node Style.
Basically, I want something that is like FTP, but hopefully faster and smarter. And of course I used my favorite programming language for it. 

## What it does

Currently, not much. It can transfer individual files, and it can list files and directories- but that's basically it. 

## What will it do

I plan to update this program to where multiple people can automatically sync with the server in real time, with the server handling everything with minimal issue or human intervention. Basically, if multiple developers want to edit a file on a remote server at the same time, I want that to be able to work without each developer having to worry about stepping on each other's toes.

I would like to add support in various IDEs for developers to be able to work on the same file/project at once and see each other's changes in real time. Failing IDE support, possibly an external webpage that says what developers are working on what file. 

It will have end-to-end encryption.

It will support transferring folders by compressing them, then transferring the single file, then decompressing the file at the other end- instead of the recursive file-by-file that most FTP solutions use.